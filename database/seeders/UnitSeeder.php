<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Unit;

class UnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $perusahaan = Unit::create([
            'name' => 'Perusahaan x',
            
        ]);

        Unit::create([
            'name' => 'Departemen Penjualan',
             'parent_unit_id' => $perusahaan->id,
            
        ]);
    }
}
