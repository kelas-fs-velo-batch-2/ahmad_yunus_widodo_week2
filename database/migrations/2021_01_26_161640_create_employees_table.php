<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->bigIncrements('id');	
            $table->string('name');	
            $table->text('date_of_birth');	
            $table->foreignId('unit_id');	
            $table->string('job_title');	
            $table->enum('employee_status', ['active','inactive','left','pension']);	
            $table->jsonb('profiles');	
            $table->softDeletes();	
            $table->timestamps();	
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
